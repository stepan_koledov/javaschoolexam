package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    @Test
    public void test2() {
        File inputFile = new File("src/test/resources/inputFile.txt");
        File outputFile = new File("src/test/resources/outputFile.txt");
        assertTrue(duplicateFinder.process(inputFile, outputFile));
        outputFile.delete();
    }

    @Test
    public void test3() {
        File inputFile = new File("src/test/resources/inputFile.txt");
        File outputFile = new File("src/test/resources/outputFile.txt");
        File templateFile = new File("src/test/resources/template.txt");
        duplicateFinder.process(inputFile, outputFile);
        Assert.assertTrue(compare(templateFile, outputFile));
    }

    @Test
    public void test4() {
        File inputFile = new File("src/test/resources/inputFile.txt");
        File outputFile = new File("src/test/resources/outputFile.txt");
        File templateFile = new File("src/test/resources/templateAppended.txt");
        duplicateFinder.process(inputFile, outputFile);
        duplicateFinder.process(inputFile, outputFile);
        Assert.assertTrue(compare(templateFile, outputFile));
    }

    private boolean compare(File templateFile, File outputFile) {
        try (Stream<String> stream1 = Files.lines(templateFile.toPath());
             Stream<String> stream2 = Files.lines(outputFile.toPath())) {
            Stack<String> stack = stream1.collect(Collectors.toCollection(Stack::new));
            Stack<String> stack1 = stream2.collect(Collectors.toCollection(Stack::new));
            return stack.equals(stack1);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            outputFile.delete();
        }
    }
}