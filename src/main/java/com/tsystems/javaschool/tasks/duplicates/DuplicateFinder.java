package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.TreeMap;
import java.util.stream.Stream;

public class DuplicateFinder {
    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || !sourceFile.exists())
            throw new IllegalArgumentException("File does not exist");
        //We have to implement the comparator to work with Cyrillic. If it is not necessary we may do not implement the comparator.
        TreeMap<String, Integer> treeMap = new TreeMap<>((s1, s2) -> {
            for (int i = 0; i < (s1.length() > s2.length() ? s2.length() : s1.length()); i++) {
                if (s1.toCharArray()[i] < s2.toCharArray()[i])
                    return -1;
                if (s1.toCharArray()[i] > s2.toCharArray()[i])
                    return 1;
            }
            return 0;
        });
        try (Stream<String> stream = Files.lines(sourceFile.toPath())) {
            stream.forEach(line -> {
                if (treeMap.containsKey(line)) treeMap.put(line, treeMap.get(line) + 1);
                else treeMap.put(line, 1);
            });
        } catch (IOException e) {
            return false;
        }
        if (!write(targetFile, treeMap)) //write collection to file
            return false;
        return true;
    }

    private boolean write(File filename, TreeMap<String, Integer> tm) {
        try (PrintWriter out = new PrintWriter(new FileOutputStream(filename, true), true)) {
            tm.entrySet().forEach(entry -> {
                out.println(String.format("%s[%d]", entry.getKey(), entry.getValue()));
                if (entry.equals(tm.lastEntry()))
                    out.println("------------");//mark end of session
            });
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
