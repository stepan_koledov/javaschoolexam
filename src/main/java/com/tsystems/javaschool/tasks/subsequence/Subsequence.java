package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        int temp = 0;
        int i, j;
        for (i = 0; i < x.size(); ++i) {
            for (j = temp; j < y.size() && !x.get(i).equals(y.get(j)); ++j)
                ;//++j while we will not find equal element in the second sequence
            if (j == y.size()) //if all the elements of the second sequence were looked
                return false;
            temp = j + 1;
        }
        return true;
    }
}
