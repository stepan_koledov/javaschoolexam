package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Pattern;

interface RegexpConst {
    String regexpDigitsWithDots = "(\\d+\\.+\\d+)|(\\.+\\d+\\.+)|(\\d+\\.+)";
    String regexpFloat = "\\d+\\.{1}\\d+";
    String regexpWrongOperators = "([\\+\\-\\*\\/]{2,})";
    String regexpNotAcceptedSymbols = "[^\\/\\+\\-\\*\\d\\.\\(\\)]";
    Pattern patternWrongOperators = Pattern.compile(regexpWrongOperators);
    Pattern patternDigitsWithDots = Pattern.compile(regexpDigitsWithDots);
    Pattern patternFloat = Pattern.compile(regexpFloat);
    Pattern patternNotAcceptedSymbols = Pattern.compile(regexpNotAcceptedSymbols);
}
