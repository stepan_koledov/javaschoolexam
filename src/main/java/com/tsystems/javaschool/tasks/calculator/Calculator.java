package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.util.regex.Matcher;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

//The algorithm moves from left to right the expression by reverse Polish notation;
//If the current element is a number or variable I put it on the top of the stack;
//If the current element is an operation, I pull out top two elements (or one, if the operation is unary) from the stack
//applied to them the operation and put the result back on the stack.
//At the end in the stack will be exactly one element - the value of the expression.
    public String evaluate(String statement) {
        if (!checkStatement(statement))
            return null;
        Stack<Float> stackOperand = new Stack<>(); //stack for operands
        Stack<Integer> stackOperator = new Stack<>(); //stack for operations
        boolean mayUnary = true;//can operator be unary
        for (int i = 0; i < statement.length(); ++i)
            if (statement.charAt(i) == '(') {
                stackOperator.push((int) '(');
                mayUnary = true; //after opened bracket, operation can be unary
            }
            //when we get closed bracket
            //we calculate all operation inside brackets
            else if (statement.charAt(i) == ')') {
                while (stackOperator.lastElement() != (int) '(')
                    if (!calculate(stackOperand, stackOperator.pop())) return null;
                stackOperator.pop();
                mayUnary = false; //after closed bracket unary operation cant be
            }
            //if current element is the operation then while
            //there is operation on the top of stack with the same or higher priority
            //we will be calculate them
            else if (isOperator(statement.charAt(i))) {
                int currentOperator = statement.charAt(i);
                if (mayUnary && isUnaryOperator(currentOperator)) // == 1)
                    currentOperator = -currentOperator; //for unary operation we put -s[i] instead of s[i]
                else if (mayUnary)
                    return null;
                while (!stackOperator.isEmpty() &&
                        (currentOperator >= 0 && getPriority(stackOperator.lastElement()) >=
                                getPriority(currentOperator) || //left associativity
                                (currentOperator < 0 &&
                                        getPriority(stackOperator.lastElement()) > getPriority(currentOperator)))) //right associativity for unary operation
                    if (!calculate(stackOperand, stackOperator.pop())) return null; //calculate
                stackOperator.push(currentOperator);
                mayUnary = true;
            } else { //if operand
                String operand = "";
                while (i < statement.length() &&
                        (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                    operand += statement.charAt(i++);
                }
                i--;
                stackOperand.push(Float.parseFloat(operand));
                mayUnary = false; //unary operation cant be after operand
            }
        //after process with input statement some operations can stay in the stack
        //which have not been calculated
        while (!stackOperator.isEmpty())
            if (!calculate(stackOperand, stackOperator.pop())) return null;
        if (stackOperand.lastElement() - (stackOperand.lastElement().intValue()) != 0)
            //if result has fractional part it must be rounding
            return String.valueOf(round(stackOperand.lastElement(), 4));
        return String.valueOf((stackOperand.lastElement().intValue()));
    }


    private int getPriority(int operator) {
        if (operator < 0) // (operator == -'+' || operator == -'-')
            return 4;
        return operator == '+' || operator == '-' ? 1 : operator == '*' || operator == '/' ? 2 : -1;
    }

    private boolean calculate(Stack<Float> st, int operator) {
        if (operator < 0) {
            float l = st.pop();
            switch (-operator) {
                case '+':
                    st.push(l);
                    break;
                case '-':
                    st.push(-l); //change sign
                    break;
            }
            return true;
        } else {
            float rightOperand = st.pop(); //pop right operand
            float leftOperand = st.pop(); //pop left operand
            //calculate and put result on the stack
            switch (operator) {
                case '+':
                    st.push(leftOperand + rightOperand);
                    break;
                case '-':
                    st.push(leftOperand - rightOperand);
                    break;
                case '*':
                    st.push(leftOperand * rightOperand);
                    break;
                case '/':
                    if (rightOperand == 0) return false;
                    st.push(leftOperand / rightOperand);
                    break;
            }
            return true;
        }
    }

    private boolean isUnaryOperator(int curr_operator) {
        return (curr_operator == '+' || curr_operator == '-');
    }

    private boolean checkStatement(String inputString) {
        return !(inputString == null
                || !checkDigits(inputString)
                || !checkOperators(inputString)
                || !checkAcceptedSymbols(inputString)
                || !checkBrackets(inputString));
    }

    private float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }

    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    private boolean checkDigits(String inputString) {
        Matcher matcher = RegexpConst.patternDigitsWithDots.matcher(inputString);
        Matcher matcherFloat;
        while (matcher.find()) {
            String str = matcher.group(0);
            matcherFloat = RegexpConst.patternFloat.matcher(str);
            if (!matcherFloat.find()) return false;
        }
        return true;
    }

    private boolean checkOperators(String inputString) {
        Matcher matcher = RegexpConst.patternWrongOperators.matcher(inputString);
        return !matcher.find();
    }

    private boolean checkAcceptedSymbols(String inputString) {
        Matcher matcher = RegexpConst.patternNotAcceptedSymbols.matcher(inputString);
        return !matcher.find();
    }

    private boolean checkBrackets(String inputString) {
        int numBrackets = 0;
        Stack stk = new Stack();
        boolean thereIsAnyOperand = false;
        for (int i = 0; i < inputString.length(); i++) {
            char c = inputString.charAt(i);
            if (Character.isDigit(c))
                thereIsAnyOperand = true;
            switch (c) {
                case '(':
                    numBrackets++;
                    stk.push(c);
                    break;
                case ')':
                    numBrackets--;
                    if (!stk.isEmpty()) {
                        char chx = (Character) stk.pop();
                        if (chx != '(')
                            return false;
                    } else
                        return false;
                    break;
            }
        }
        return thereIsAnyOperand && numBrackets == 0;
    }

}
